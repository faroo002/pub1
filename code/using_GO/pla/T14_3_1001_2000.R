#############################  FUNCTIONS  ##############################################
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
`%not_in%` <- purrr::negate(`%in%`)
# Function to check whether package is installed
is.installed <- function(mypkg)
{
    is.element(mypkg, installed.packages()[,1])
} 
########################################################################################

if (rstudioapi::isAvailable()) {
  if (require('rstudioapi') != TRUE) {
    install.packages('rstudioapi')
  }else{
    library(rstudioapi) # load it
  }
 wdir <- dirname(getActiveDocumentContext()$path)
}else{
 wdir <- getwd()
}
this_file = gsub("--file=", "", commandArgs()[grepl("--file", commandArgs())])
if (length(this_file) > 0){
  wd <- paste(head(strsplit(this_file, '[/|\]')[[1]], -1), collapse = .Platform$file.sep)
}else{
  wd <- dirname(rstudioapi::getSourceEditorContext()$path)
}

setwd(paste0(wdir,"/",wd,"/8old"));
########################################################################################

#if (!is.installed("backports"))
#{
#  install.packages("backports")
#}
#if (!is.installed("devtools"))
#{
#  install.packages("devtools")
#}
#library("devtools")
#if (!is.installed("qgg"))
#{
#  options(devtools.install.args=" --no-multiargs")
#  devtools::install_github("psoerensen/qgg")
#}
message("Loading required packages...")
library(GO.db)
library(org.At.tair.db)
#columns(org.At.tair.db)
library('qgg')
library('dplyr')
###########################################################################################
message("Loading data...")
MAC_matrix_with_header=readRDS(file="../../../../data/MAC_matrix_with_header.rds")
accessions=readRDS(file="../../../../data/accessions.rds")
all_nucl_genes_bed=readRDS(file="../../../../data/all_nucl_genes_bed.rds")
ath_all_new_maf_ldpruned_map=readRDS(file="../../../../data/ath_all_new_maf_ldpruned_map.rds")
W=readRDS(file="../../../../data/W.rds")
load(file="../../../../data/G.Rdata")
MAC_df=readRDS(file="../../../../data/MAC_df.rds")
load(file="../../../../data/Pheno_pla.Rdata")
load(file="../../../../data/geno_pheno_pla.Rdata")
load(file="../../../../data/pheno_df_pla.Rdata")
go_all_genes_number=readRDS(file="../../../../data/priors/go/go_all_genes_number.rds")		
go_all_markers_number=readRDS(file="../../../../data/priors/go/go_all_markers_number.rds")	
go_all_markers=readRDS(file="../../../../data/priors/go/go_all_markers.rds")			
markerSets=readRDS(file="../../../../data/priors/go/markerSets.rds")				
setsGF=readRDS(file="../../../../data/priors/go/setsGF.rds")					
rsetsGF=readRDS(file="../../../../data/priors/go/rsetsGF.rds")					
nsets=readRDS(file="../../../../data/priors/go/nsets.rds")					
nsnps=readRDS(file="../../../../data/priors/go/nsnps.rds")					
GF=readRDS(file="../../../../data/priors/go/GF.rds")						
rGF=readRDS(file="../../../../data/priors/go/rGF.rds")							
########################################################################################
#filter
n=length(go_all_genes_number[go_all_markers_number>0])
go_all_markers_filtered<-go_all_markers[go_all_markers_number>0]
GF_filtered<-GF[names(go_all_markers_filtered)]
rGF_filtered<-rGF[names(go_all_markers_filtered)]
GF_filtered<-GF_filtered[1001:2000]
rGF_filtered<-rGF_filtered[1001:2000]
###########################################################################################
cycles=10; #gblup_validate=matrix(unlist(gblup_validate[[r]]),round(nrow(pheno_df_pla)/n_folds),byrow=TRUE); 
 gblup_validate=readRDS(file="../../../GBLUP/pla/8fold/gblup_validate_all.rds")
n_folds <- 8

T14_3_gblup_variances_all=rep(list(list()),cycles)
T14_3_gblup_prediction_all=rep(list(list()),cycles)
T14_3_gfblup_variances_all=rep(list(list()),cycles)
T14_3_gfblup_prediction_all=rep(list(list()),cycles)
T14_3_gfblup_validate_all=rep(list(list()),cycles)

for(r in 1:cycles)
{
	message(paste("cycle#",r,"..."))
	a=as.numeric(Sys.time())
	set.seed(a)  #generate random folds sets everytime
	folds_index <- sample(rep(1:n_folds, length.out = nrow(pheno_df_pla)))
	validate=matrix(unlist(gblup_validate[[r]]),round(nrow(pheno_df_pla)/n_folds),byrow=TRUE); #validate=matrix(nrow=round(nrow(pheno_df_pla)/n_folds),ncol=n_folds)
	for(i in c(1:n_folds))
	{
		#validate[,i]=which(folds_index==i)
	}
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	message ("T14_3...")
	y=pheno_df_pla$T14_3
	fm <- y ~ 1*y
	X <- model.matrix(fm)  
	n <- length(y)
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GBLUP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	result = tryCatch({
		var <- greml(y = y, X = X, GRM = list(G=G),ncores=1)
		pred <- greml(y = y, X = X, GRM = list(G=G), validate = validate,ncores=1)
		T14_3_gblup_variances_all[[r]]<-var
		T14_3_gblup_prediction_all[[r]]<-pred
	}, error = function(e) {
	 	T14_3_gblup_variances_all[[r]]<-list() 
	 	T14_3_gblup_prediction_all[[r]]<-list()
	})  #try catch ends
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GFBLUP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	var <-rep(list(list()),length(GF_filtered))
	pred <-rep(list(list()),length(GF_filtered))
	names(var)<-(names(GF_filtered))
	names(pred)<-(names(GF_filtered))
	for(k in c(1:length(GF_filtered)))
	{
		message(paste("GO#",k,"..."))
		x=(r*k)+((r-1)*(length(GF_filtered)-k))
		result = tryCatch({
			var[[k]] <- greml(y = y, X = X, GRM = c(GF_filtered[k],rGF_filtered[k]),ncores=1)
			pred[[k]] <- greml(y = y, X = X, GRM = c(GF_filtered[k],rGF_filtered[k]), validate = validate,ncores=1)
		}, error = function(e) {
			var[[k]]<-list()
			pred[[k]]<-list()
			#print("gfblup error")
		})  #try catch ends
	}
	T14_3_gfblup_variances_all[[r]]<-var
	T14_3_gfblup_prediction_all[[r]]<-pred
	T14_3_gfblup_validate_all[[r]]<-list(validate)	
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}
saveRDS(T14_3_gblup_variances_all,"T14_3_gblup_variances_all_1001_2000.rds")
saveRDS(T14_3_gblup_prediction_all,"T14_3_gblup_prediction_all_1001_2000.rds")
saveRDS(T14_3_gfblup_variances_all,"T14_3_gfblup_variances_all_1001_2000.rds")
saveRDS(T14_3_gfblup_prediction_all,"T14_3_gfblup_prediction_all_1001_2000.rds")
saveRDS(T14_3_gfblup_validate_all,"T14_3_gfblup_validate_all_1001_2000.rds")

##################################################################################################################################
