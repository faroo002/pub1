#############################  FUNCTIONS  ##############################################
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
`%not_in%` <- purrr::negate(`%in%`)
# Function to check whether package is installed
is.installed <- function(mypkg)
{
    is.element(mypkg, installed.packages()[,1])
} 
########################################################################################

setwd("/mnt/LTR_userdata/faroo002/arabidopsis/GP/using_conda/using_GO/psii/8fold/")

#if (!is.installed("backports"))
#{
#  install.packages("backports")
#}
#if (!is.installed("devtools"))
#{
#  install.packages("devtools")
#}
#library("devtools")
#if (!is.installed("qgg"))
#{
#  options(devtools.install.args=" --no-multiargs")
#  devtools::install_github("psoerensen/qgg")
#}
message("Loading required packages...")
library(GO.db)
library(org.At.tair.db)
#columns(org.At.tair.db)
library('qgg')
library('dplyr')
###########################################################################################
message("Loading data...")
MAC_matrix_with_header=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/data/MAC_matrix_with_header.rds")
accessions=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/data/accessions.rds")
all_nucl_genes_bed=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/data/all_nucl_genes_bed.rds")
ath_all_new_maf_ldpruned_map=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/data/ath_all_new_maf_ldpruned_map.rds")
W=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/data/W.rds")
load(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/data/G.Rdata")
Pheno=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/data/Pheno.rds")
geno_pheno=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/data/geno_pheno.rds")
MAC_df=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/data/MAC_df.rds")
pheno_df=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/data/pheno_df.rds")
go_all_genes_number=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/go/go_all_genes_number.rds")		#7432
go_all_markers_number=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/go/go_all_markers_number.rds")	#7432
go_all_markers=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/go/go_all_markers.rds")			#7432
markerSets=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/go/markerSets.rds")				#7297	excluding those go terms with zero markers
setsGF=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/go/setsGF.rds")					#7297	excluding those go terms with zero markers
rsetsGF=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/go/rsetsGF.rds")					#7297	excluding those go terms with zero markers
nsets=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/go/nsets.rds")					#7297	excluding those go terms with zero markers
nsnps=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/go/nsnps.rds")					#7297	excluding those go terms with zero markers
GF=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/go/GF.rds")						#7297	excluding those go terms with zero markers
rGF=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/priors/go/rGF.rds")						#7297	excluding those go terms with zero markers	
########################################################################################
n=length(go_all_genes_number[go_all_markers_number>0])
go_all_markers_filtered<-go_all_markers[go_all_markers_number>0]
GF_filtered<-GF[names(go_all_markers_filtered)]
rGF_filtered<-rGF[names(go_all_markers_filtered)]
###########################################################################################
cycles=10; #gblup_validate=matrix(unlist(gblup_validate[[r]]),round(nrow(pheno_df)/n_folds),byrow=TRUE); 
 gblup_validate=readRDS(file="/mnt/LTR_userdata/faroo002/arabidopsis/GP/using_conda/GBLUP/psii/8fold/gblup_validate_all.rds")
n_folds <- 8

L1_1_gblup_variances_all=rep(list(list()),cycles)
L1_1_gblup_prediction_all=rep(list(list()),cycles)
L1_1_gfblup_variances_all=rep(list(list()),cycles)
L1_1_gfblup_prediction_all=rep(list(list()),cycles)
L1_1_gfblup_validate_all=rep(list(list()),cycles)

for(r in 1:cycles)
{
	message(paste("cycle#",r,"..."))
	a=as.numeric(Sys.time())
	set.seed(a)  #generate random folds sets everytime
	folds_index <- sample(rep(1:n_folds, length.out = nrow(pheno_df)))
	validate=matrix(unlist(gblup_validate[[r]]),round(nrow(pheno_df)/n_folds),byrow=TRUE); #validate=matrix(nrow=round(nrow(pheno_df)/n_folds),ncol=n_folds)
	for(i in c(1:n_folds))
	{
		#validate[,i]=which(folds_index==i)
	}
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	message ("L1_1...")
	y=1000000*pheno_df$L1_1
	fm <- y ~ 1*y
	X <- model.matrix(fm)  
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GBLUP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	result = tryCatch({
		var <- greml(y = y, X = X, GRM = list(G=G),ncores=1)
		pred <- greml(y = y, X = X, GRM = list(G=G), validate = validate,ncores=1)
		L1_1_gblup_variances_all[[r]]<-var
		L1_1_gblup_prediction_all[[r]]<-pred
	}, error = function(e) {
	 	L1_1_gblup_variances_all[[r]]<-list() 
	 	L1_1_gblup_prediction_all[[r]]<-list()
	})  #try catch ends
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GFBLUP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	var <-rep(list(list()),length(GF_filtered))
	pred <-rep(list(list()),length(GF_filtered))
	names(var)<-(names(GF_filtered))
	names(pred)<-(names(GF_filtered))
	for(k in c(1:length(GF_filtered)))
	{
		message(paste("GO#",k,"..."))
		x=(r*k)+((r-1)*(length(GF_filtered)-k))
		result = tryCatch({
			var[[k]] <- greml(y = y, X = X, GRM = c(GF_filtered[k],rGF_filtered[k]),ncores=1)
			pred[[k]] <- greml(y = y, X = X, GRM = c(GF_filtered[k],rGF_filtered[k]), validate = validate,ncores=1)
		}, error = function(e) {
			var[[k]]<-list()
			pred[[k]]<-list()
			#print("gfblup error")
		})  #try catch ends
	}
	L1_1_gfblup_variances_all[[r]]<-var
	L1_1_gfblup_prediction_all[[r]]<-pred
	L1_1_gfblup_validate_all[[r]]<-list(validate)	
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}
saveRDS(L1_1_gblup_variances_all,"L1_1_gblup_variances_all.rds")
saveRDS(L1_1_gblup_prediction_all,"L1_1_gblup_prediction_all.rds")
saveRDS(L1_1_gfblup_variances_all,"L1_1_gfblup_variances_all.rds")
saveRDS(L1_1_gfblup_prediction_all,"L1_1_gfblup_prediction_all.rds")
saveRDS(L1_1_gfblup_validate_all,"L1_1_gfblup_validate_all.rds")

##################################################################################################################################
