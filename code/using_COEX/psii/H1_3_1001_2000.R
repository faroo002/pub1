
#############################  FUNCTIONS  ##############################################
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
`%not_in%` <- purrr::negate(`%in%`)
# Function to check whether package is installed
is.installed <- function(mypkg)
{
    is.element(mypkg, installed.packages()[,1])
} 
########################################################################################
if (rstudioapi::isAvailable()) {
  if (require('rstudioapi') != TRUE) {
    install.packages('rstudioapi')
  }else{
    library(rstudioapi) # load it
  }
 wdir <- dirname(getActiveDocumentContext()$path)
}else{
 wdir <- getwd()
}
this_file = gsub("--file=", "", commandArgs()[grepl("--file", commandArgs())])
if (length(this_file) > 0){
  wd <- paste(head(strsplit(this_file, '[/|\]')[[1]], -1), collapse = .Platform$file.sep)
}else{
  wd <- dirname(rstudioapi::getSourceEditorContext()$path)
}

setwd(paste0(wdir,"/",wd,"/8old"));
########################################################################################
message("Loading required packages...")
library('qgg')
library('dplyr')
###########################################################################################
message("Loading data...")
MAC_matrix_with_header=readRDS(file="../../../../data/MAC_matrix_with_header.rds")
accessions=readRDS(file="../../../../data/accessions.rds")
all_nucl_genes_bed=readRDS(file="../../../../data/all_nucl_genes_bed.rds")
ath_all_new_maf_ldpruned_map=readRDS(file="../../../../data/ath_all_new_maf_ldpruned_map.rds")
Pheno=readRDS(file="../../../../data/Pheno.rds")
geno_pheno=readRDS(file="../../../../data/geno_pheno.rds")
MAC_df=readRDS(file="../../../../data/MAC_df.rds")
pheno_df=readRDS(file="../../../../data/pheno_df.rds")
load(file="../../../../data/G.Rdata")
all_coexpression_clusters <-as.data.frame(read.table(file ="../../../../data/priors/coexpression/ath_gene_clusters.txt", header=TRUE, sep="",strip.white=TRUE))


coex_markers_number=readRDS(file="../../../../data/priors/coexpression/coex_markers_number_1001_2000.rds")
coex_markers=readRDS(file="../../../../data/priors/coexpression/coex_markers_1001_2000.rds")
markerSets=readRDS(file="../../../../data/priors/coexpression/markerSets_1001_2000.rds")
setsGF=readRDS(file="../../../../data/priors/coexpression/setsGF_1001_2000.rds")
rsetsGF=readRDS(file="../../../../data/priors/coexpression/rsetsGF_1001_2000.rds")
nsets=readRDS(file="../../../../data/priors/coexpression/nsets_1001_2000.rds")
nsnps=readRDS(file="../../../../data/priors/coexpression/nsnps_1001_2000.rds")
GF=readRDS(file="../../../../data/priors/coexpression/GF_1001_2000.rds")
rGF=readRDS(file="../../../../data/priors/coexpression/rGF_1001_2000.rds")
###########################################################################################
cycles=10; #gblup_validate=matrix(unlist(gblup_validate[[r]]),round(nrow(pheno_df)/n_folds),byrow=TRUE); 
 gblup_validate=readRDS(file="../../../GBLUP/psii/8fold/gblup_validate_all.rds")
n_folds <- 8

H1_3_gblup_variances_all=rep(list(list()),cycles)
H1_3_gblup_prediction_all=rep(list(list()),cycles)
H1_3_gfblup_variances_all=rep(list(list()),cycles)
H1_3_gfblup_prediction_all=rep(list(list()),cycles)
H1_3_gfblup_validate_all=rep(list(list()),cycles)

for(r in 1:cycles)
{
	message(paste("cycle#",r,"..."))
	a=as.numeric(Sys.time())
	set.seed(a)  #generate random folds sets everytime
	folds_index <- sample(rep(1:n_folds, length.out = nrow(pheno_df)))
	validate=matrix(unlist(gblup_validate[[r]]),round(nrow(pheno_df)/n_folds),byrow=TRUE); #validate=matrix(nrow=round(nrow(pheno_df)/n_folds),ncol=n_folds)
	for(i in c(1:n_folds))
	{
		#validate[,i]=which(folds_index==i)
	}
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	message ("H1_3...")
	y=1000000*pheno_df$H1_3
	fm <- y ~ 1*y
	X <- model.matrix(fm)  
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GBLUP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	result = tryCatch({
		var <- greml(y = y, X = X, GRM = list(G=G),ncores=1)
		pred <- greml(y = y, X = X, GRM = list(G=G), validate = validate,ncores=20)
		H1_3_gblup_variances_all[[r]]<-var
		H1_3_gblup_prediction_all[[r]]<-pred
	}, error = function(e) {
	 	H1_3_gblup_variances_all[[r]]<-list() 
	 	H1_3_gblup_prediction_all[[r]]<-list()
	})  #try catch ends
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GFBLUP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	var <-rep(list(list()),length(GF))
	pred <-rep(list(list()),length(GF))
	names(var)<-(names(GF))
	names(pred)<-(names(GF))
	for(k in c(1:length(GF)))
	{
		message(paste("Cluster#",k,"..."))
		result = tryCatch({
			var[[k]] <- greml(y = y, X = X, GRM = c(GF[k],rGF[k]),ncores=20)
			pred[[k]] <- greml(y = y, X = X, GRM = c(GF[k],rGF[k]), validate = validate,ncores=20)
		}, error = function(e) {
			var[[k]]<-list()
			pred[[k]]<-list()
			#print("gfblup error")
		})  #try catch ends
	}
	H1_3_gfblup_variances_all[[r]]<-var
	H1_3_gfblup_prediction_all[[r]]<-pred
	H1_3_gfblup_validate_all[[r]]<-list(validate)	
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}
saveRDS(H1_3_gblup_variances_all,"H1_3_gblup_variances_all_1001_2000.rds")
saveRDS(H1_3_gblup_prediction_all,"H1_3_gblup_prediction_all_1001_2000.rds")
saveRDS(H1_3_gfblup_variances_all,"H1_3_gfblup_variances_all_1001_2000.rds")
saveRDS(H1_3_gfblup_prediction_all,"H1_3_gfblup_prediction_all_1001_2000.rds")
saveRDS(H1_3_gfblup_validate_all,"H1_3_gfblup_validate_all_1001_2000.rds")

##################################################################################################################################
