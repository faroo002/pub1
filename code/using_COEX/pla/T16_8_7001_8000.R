#############################  FUNCTIONS  ##############################################
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
`%not_in%` <- purrr::negate(`%in%`)
# Function to check whether package is installed
is.installed <- function(mypkg)
{
    is.element(mypkg, installed.packages()[,1])
} 
########################################################################################
if (rstudioapi::isAvailable()) {
  if (require('rstudioapi') != TRUE) {
    install.packages('rstudioapi')
  }else{
    library(rstudioapi) # load it
  }
 wdir <- dirname(getActiveDocumentContext()$path)
}else{
 wdir <- getwd()
}
this_file = gsub("--file=", "", commandArgs()[grepl("--file", commandArgs())])
if (length(this_file) > 0){
  wd <- paste(head(strsplit(this_file, '[/|\]')[[1]], -1), collapse = .Platform$file.sep)
}else{
  wd <- dirname(rstudioapi::getSourceEditorContext()$path)
}

setwd(paste0(wdir,"/",wd,"/8old"));
########################################################################################
message("Loading required packages...")
library('qgg')
library('dplyr')
###########################################################################################
message("Loading data...")
MAC_matrix_with_header=readRDS(file="../../../../data/MAC_matrix_with_header.rds")
accessions=readRDS(file="../../../../data/accessions.rds")
all_nucl_genes_bed=readRDS(file="../../../../data/all_nucl_genes_bed.rds")
ath_all_new_maf_ldpruned_map=readRDS(file="../../../../data/ath_all_new_maf_ldpruned_map.rds")
W=readRDS(file="../../../../data/W.rds")
load(file="../../../../data/G.Rdata")
MAC_df=readRDS(file="../../../../data/MAC_df.rds")
load(file="../../../../data/Pheno_pla.Rdata")
load(file="../../../../data/geno_pheno_pla.Rdata")
load(file="../../../../data/pheno_df_pla.Rdata")

all_coexpression_clusters <-as.data.frame(read.table(file ="../../../../data/priors/coexpression/ath_gene_clusters.txt", header=TRUE, sep="",strip.white=TRUE))
coex_markers_number=readRDS(file="../../../../data/priors/coexpression/coex_markers_number_7001_8000.rds")
coex_markers=readRDS(file="../../../../data/priors/coexpression/coex_markers_7001_8000.rds")
markerSets=readRDS(file="../../../../data/priors/coexpression/markerSets_7001_8000.rds")
setsGF=readRDS(file="../../../../data/priors/coexpression/setsGF_7001_8000.rds")
rsetsGF=readRDS(file="../../../../data/priors/coexpression/rsetsGF_7001_8000.rds")
nsets=readRDS(file="../../../../data/priors/coexpression/nsets_7001_8000.rds")
nsnps=readRDS(file="../../../../data/priors/coexpression/nsnps_7001_8000.rds")
GF=readRDS(file="../../../../data/priors/coexpression/GF_7001_8000.rds")
rGF=readRDS(file="../../../../data/priors/coexpression/rGF_7001_8000.rds")	
########################################################################################
cycles=10; #gblup_validate=matrix(unlist(gblup_validate[[r]]),round(nrow(pheno_df_pla)/n_folds),byrow=TRUE); 
 gblup_validate=readRDS(file="../../../GBLUP/pla/8fold/gblup_validate_all.rds")
n_folds <- 8

T16_8_gblup_variances_all=rep(list(list()),cycles)
T16_8_gblup_prediction_all=rep(list(list()),cycles)
T16_8_gfblup_variances_all=rep(list(list()),cycles)
T16_8_gfblup_prediction_all=rep(list(list()),cycles)
T16_8_gfblup_validate_all=rep(list(list()),cycles)

for(r in 1:cycles)
{
	message(paste("cycle#",r,"..."))
	a=as.numeric(Sys.time())
	set.seed(a)  #generate random folds sets everytime
	folds_index <- sample(rep(1:n_folds, length.out = nrow(pheno_df_pla)))
	validate=matrix(unlist(gblup_validate[[r]]),round(nrow(pheno_df_pla)/n_folds),byrow=TRUE); #validate=matrix(nrow=round(nrow(pheno_df_pla)/n_folds),ncol=n_folds)
	for(i in c(1:n_folds))
	{
		#validate[,i]=which(folds_index==i)
	}
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	message ("T16_8...")
	y=pheno_df_pla$T16_8
	fm <- y ~ 1*y
	X <- model.matrix(fm)  
	n <- length(y)
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GBLUP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	result = tryCatch({
		var <- greml(y = y, X = X, GRM = list(G=G),ncores=1)
		pred <- greml(y = y, X = X, GRM = list(G=G), validate = validate,ncores=15)
		T16_8_gblup_variances_all[[r]]<-var
		T16_8_gblup_prediction_all[[r]]<-pred
	}, error = function(e) {
	 	T16_8_gblup_variances_all[[r]]<-list() 
	 	T16_8_gblup_prediction_all[[r]]<-list()
	})  #try catch ends
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GFBLUP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	var <-rep(list(list()),length(GF))
	pred <-rep(list(list()),length(GF))
	names(var)<-(names(GF))
	names(pred)<-(names(GF))
	for(k in c(1:length(GF)))
	{
		message(paste("cluster#",k,"..."))
		x=(r*k)+((r-1)*(length(GF)-k))
		result = tryCatch({
			var[[k]] <- greml(y = y, X = X, GRM = c(GF[k],rGF[k]),ncores=15)
			pred[[k]] <- greml(y = y, X = X, GRM = c(GF[k],rGF[k]), validate = validate,ncores=15)
		}, error = function(e) {
			var[[k]]<-list()
			pred[[k]]<-list()
			#print("gfblup error")
		})  #try catch ends
	}
	T16_8_gfblup_variances_all[[r]]<-var
	T16_8_gfblup_prediction_all[[r]]<-pred
	T16_8_gfblup_validate_all[[r]]<-list(validate)	
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}
saveRDS(T16_8_gblup_variances_all,"T16_8_gblup_variances_all_7001_8000.rds")
saveRDS(T16_8_gblup_prediction_all,"T16_8_gblup_prediction_all_7001_8000.rds")
saveRDS(T16_8_gfblup_variances_all,"T16_8_gfblup_variances_all_7001_8000.rds")
saveRDS(T16_8_gfblup_prediction_all,"T16_8_gfblup_prediction_all_7001_8000.rds")
saveRDS(T16_8_gfblup_validate_all,"T16_8_gfblup_validate_all_7001_8000.rds")

##################################################################################################################################
