cd ../data
#The MAC matrix contains each entry corresponding to minor allele counts. i.e. if a genotype contains tt, where t is minor allele, then it will have 2 and if Tt, then it will have 1 and if TT, then this matrix will have 0 at that entry.
cat ath_all_new_maf_ldpruned.map | awk 'BEGIN{printf "accession"}{printf "\t%s",$2;}END{printf "\n";}' >test1
cat ath_all_new_maf_ldpruned.ped | awk '{printf "%s",$1; for(i=7;i<=NF;i++){if($i=="11"){printf "\t2";}else if($i=="22"){printf "\t0";}else {printf "\t1";}}printf "\n";}'>test2
cat test1 test2 > MAC_matrix
rm test1
rm test2

#The M matrix contains genotypes scored simply i.e. 11=Minor Allele Homozygous genotype coded as '-1', 12=Hetozygous coded as '0', and 22=Major allele homozygous coded as '1'
#cat ath_all_recode_compound_genotypes.ped | awk '{printf "%s",$1; for(i=7;i<=NF;i++){if($i=="11"){printf " -1";}else if($i=="22"){printf " 1";}else {printf " 0";}}printf "\n";}'>M_matrix


